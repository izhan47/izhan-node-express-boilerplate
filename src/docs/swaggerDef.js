const { version } = require('../../package.json');
const config = require('../config/config');

const swaggerDef = {
  openapi: '3.0.0',
  info: {
    title: 'izhan-node-express-boilerplate API documentation',
    version,
    license: {
      name: 'MIT',
      url: 'https://gitlab.com/izhan47/izhan-node-express-boilerplate/blob/master/LICENSE',
    },
  },
  servers: [
    {
      url: `http://localhost:${config.port}/v1`,
    },
  ],
};

module.exports = swaggerDef;
